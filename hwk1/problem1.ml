let rec clone ((e, n) : 'a * int) : 'a list =
  match n with
  | 0 -> []
  | 1 -> [e]
  | _ -> e :: clone(e, n-1);;
