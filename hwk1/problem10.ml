let rec rotate ((l, n) : 'a list * int) : 'a list =
  match n with
  | 0 -> l
  | _ -> 
      match l with
      | [] -> []
      | head::tail -> rotate(tail @ [head], n-1);;
