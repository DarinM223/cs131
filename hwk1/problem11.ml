let rec dec2bin (n: int) : int list =
  match n with
  | 0 -> [0]
  | 1 -> [1]
  | _ when n > 0 -> dec2bin(n/2) @ [n mod 2];;
