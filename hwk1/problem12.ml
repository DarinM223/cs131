let rec pairify (l : 'a list) : ('a * 'a) list =
  match l with
  | [] -> []
  | [elem] -> []
  | elem1::elem2::tail -> (elem1, elem2)::pairify(tail);;
