let rec rev (l: 'a list) : 'a list = 
  match l with
  | head::tail -> rev(tail)@[head]
  | [] -> [];;
