let rec clone ((e, n) : 'a * int) : 'a list =
  match n with
  | 0 -> []
  | 1 -> [e]
  | _ -> e :: clone(e, n-1);;

let rec rev (l: 'a list) : 'a list = 
  match l with
  | head::tail -> rev(tail)@[head]
  | [] -> [];;

let fastRev (l : 'a list) : 'a list =
  let rec revHelper (remain, sofar) = 
    match remain with
    | [] -> sofar
    | head::tail -> revHelper(tail, head::sofar)

in revHelper(l, [])

