let rec tails (l : 'a list) : 'a list list = 
  match l with
  | [] -> [[]]
  | head::tail -> (head::tail) :: tails(tail);;
