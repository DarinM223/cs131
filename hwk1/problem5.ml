let rec penultimate (l : 'a list) : 'a option =
  match l with
  | [] -> None
  | [a] -> None
  | [a; b] -> Some a
  | head::tail -> penultimate(tail)
