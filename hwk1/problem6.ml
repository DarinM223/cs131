let rec flatten (l: 'a list list) : 'a list =
  match l with
  | [] -> []
  | [a] -> a
  | head::tail -> head @ flatten(tail)
