let intOfDigits (l: int list) : int =
  let rec calcDigits((l, n): int list * int) : int =
    match l with
    | [] -> 0 
    | [elem] -> n*10 + elem
    | head::tail -> calcDigits(tail, n*10 + head)
in calcDigits(l, 0)
