let rec merge ((l1, l2): int list * int list) : int list = 
 match (l1, l2) with
 | ([], []) -> []
 | ([], head::tail) -> head::merge([], tail)
 | (head::tail, []) -> head::merge(tail, [])
 | (head1::tail1, head2::tail2) ->
     if (head1 < head2) then
       head1::merge(tail1, head2::tail2)
     else
       head2::merge(head1::tail1, tail2)
