let rec encode (l: 'a list) : (int * 'a) list = 
  match l with
  | [] -> []
  | head::tail -> 
      match (encode tail) with
      | [] -> (1, head)::[]
      | (times, data)::tail2 -> 
          if data = head then
            ((times+1, data)::tail2)
          else
            ((1, head)::(times, data)::tail2)
