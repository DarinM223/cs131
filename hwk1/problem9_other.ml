let rec encode (l: 'a list) : (int * 'a) list = 
  match l with
  | [] -> []
  | first::rest -> 
      match (encode rest) with
      | [] -> (1, first)::[]
      | (count, v)::rest2 -> 
          if v = first then
            ((count+1, v)::rest2)
          else
            ((1, first)::(count, v)::rest2)
