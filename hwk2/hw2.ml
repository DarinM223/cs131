
(* Name: Darin Minamoto

   UID: 704-140-102

   Others With Whom I Discussed Things:

   Other Resources I Consulted:
   
*)

(* Problem 1a
   doubleAllPos : int list -> int list *)

let doubleAllPos l = List.map (function elem -> if elem > 0 then elem*2 else elem) l

(* Problem 1b
   unzip : ('a * 'b) list -> 'a list * 'b list *)

let unzip l = List.fold_right  (fun elem rest -> 
  match elem with
  | (x, y) -> 
      match rest with
      | (l1, l2) -> (x::l1, y::l2)
  ) l ([], [])

(* Problem 1c
   encode : 'a list -> (int * 'a) list *)

let encode l = List.fold_right (fun elem rest ->
  match rest with
  | [] -> (1, elem)::[]
  | (times, data)::tail -> 
      if data = elem then
        ((times+1, data)::tail)
      else 
        ((1, elem)::rest)) l []

(* Problem 1d
   intOfDigits : int list -> int *)

let intOfDigits l = List.fold_left (fun acc elem -> acc*10 + elem) 0 l

(* Problem 2a
   map2 : ('a -> 'b -> 'c) -> 'a list -> 'b list -> 'c list *)

let rec map2 f l1 l2 = 
  match (l1, l2) with
  | ([], []) -> []
  | (head1::tail1, head2::tail2) -> 
      (f head1 head2)::(map2 f tail1 tail2)

(* Problem 2b
   zip : 'a list * 'b list -> ('a * 'b) list *)

let zip (a, b) = map2 (fun x y -> (x, y)) a b

(* Problem 2c
   foldn : (int -> 'a -> 'a) -> int -> 'a -> 'a *)

let rec foldn f n b =
  match n with
  | 0 -> b
  | 1 -> b
  | _ -> f n (foldn f (n-1) b)

(* Problem 2d
   clone : 'a * int -> 'a list *)

let clone (e, n) = foldn (fun x rest -> e::rest) (n+1) []

(* Problem 3a
   empty1: unit -> ('a * 'b) list
   put1: 'a -> 'b -> ('a * 'b) list -> ('a * 'b) list
   get1: 'a -> ('a * 'b) list -> 'b option
*)  

let empty1: unit -> ('a * 'b) list = fun _ -> []

let put1: 'a -> 'b -> ('a * 'b) list -> ('a * 'b) list = (fun a b l -> (a, b)::l)

let rec get1 e l = 
  match l with
  | [] -> None
  | (key, value)::tail -> if key = e then Some value else get1 e tail

(* Problem 3b
   empty2: unit -> ('a,'b) dict2
   put2: 'a -> 'b -> ('a,'b) dict2 -> ('a,'b) dict2
   get2: 'a -> ('a,'b) dict2 -> 'b option
*)  
    
type ('a,'b) dict2 = Empty | Entry of 'a * 'b * ('a,'b) dict2

let empty2: unit -> ('a, 'b) dict2 = (fun _ -> Empty)

let put2: 'a -> 'b -> ('a, 'b) dict2 -> ('a, 'b) dict2 = fun key value dict ->
  Entry(key, value, dict)

let rec get2: 'a -> ('a, 'b) dict2 -> 'b option = fun key dict ->
  match dict with
  | Empty -> None 
  | Entry(k, v, rest) -> if k = key then Some v else get2 key rest
	
(* Problem 3c
   empty3: unit -> ('a,'b) dict3
   put3: 'a -> 'b -> ('a,'b) dict3 -> ('a,'b) dict3
   get3: 'a -> ('a,'b) dict3 -> 'b option
*)  

type ('a,'b) dict3 = ('a -> 'b option)

let empty3: unit -> ('a, 'b) dict3 = (fun _ -> (fun x -> None))

let put3: 'a -> 'b -> ('a, 'b) dict3 -> ('a,'b) dict3 = (fun key value dict ->
  fun x -> 
    if x = key then Some value
    else dict x )

let get3: 'a -> ('a, 'b) dict3 -> 'b option = (fun key dict -> dict key)
