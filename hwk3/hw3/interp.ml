(* Name: Darin Minamoto 

   UID: 704-140-102

   Others With Whom I Discussed Things:

   Other Resources I Consulted:
   
*)

(* EXCEPTIONS *)

(* This is a marker for places in the code that you have to fill in.
   Your completed assignment should never raise this exception. *)
exception ImplementMe of string

(* This exception is thrown when a type error occurs during evaluation
   (e.g., attempting to invoke something that's not a function).
*)
exception DynamicTypeError

(* This exception is thrown when pattern matching fails during evaluation. *)  
exception MatchFailure  

(* EVALUATION *)

(* See if a value matches a given pattern.  If there is a match, return
   an environment for any name bindings in the pattern.  If there is not
   a match, raise the MatchFailure exception.
*)
let rec patMatch (pat:mopat) (value:movalue) : moenv =
  match (pat, value) with
    (* an integer pattern matches an integer only when they are the same constant;
   no variables are declared in the pattern so the returned environment is empty *)
    (IntPat(i), IntVal(j)) when i=j -> Env.empty_env()
  | (BoolPat(i), BoolVal(j)) when i=j -> Env.empty_env()
  | (WildcardPat, _) -> Env.empty_env()
  | (VarPat(varname), value) -> Env.add_binding varname value (Env.empty_env())
  | (NilPat, ListVal(l)) when l = NilVal -> Env.empty_env()
  | (ConsPat(pat1, pat2), ListVal(ConsVal(head, tail))) -> 
      Env.combine_envs (patMatch pat1 head) (patMatch pat2 (ListVal(tail)) )
  | _ -> raise MatchFailure

  
(* Evaluate an expression in the given environment and return the
   associated value.  Raise a MatchFailure if pattern matching fails.
   Raise a DynamicTypeError if any other kind of error occurs (e.g.,
   trying to add a boolean to an integer) which prevents evaluation
   from continuing.
*)
let rec evalExpr (e:moexpr) (env:moenv) : movalue =
  match e with
    (* an integer constant evaluates to itself *)
    IntConst(i) -> IntVal(i)
  | BoolConst(b) -> BoolVal(b)
  | Nil -> ListVal(NilVal)
  | Var(varname) -> (try (Env.lookup varname env) with Env.NotBound -> raise DynamicTypeError)
  | BinOp(left, operation, right) -> 
    (* First five operators only apply to integers *)
    (match operation with
    | Plus -> 
      (match ((evalExpr left env), (evalExpr right env)) with
      | (IntVal(a), IntVal(b)) -> IntVal(a + b)
      | _ -> raise DynamicTypeError)
    | Minus -> 
      (match ((evalExpr left env), (evalExpr right env)) with
      | (IntVal(a), IntVal(b)) -> IntVal(a - b)
      | _ -> raise DynamicTypeError)
    | Times -> 
      (match ((evalExpr left env), (evalExpr right env)) with
      | (IntVal(a), IntVal(b)) -> IntVal(a * b)
      | _ -> raise DynamicTypeError)
    | Eq -> 
      (match ((evalExpr left env), (evalExpr right env)) with
      | (IntVal(a), IntVal(b)) -> BoolVal(a = b)
      | _ -> raise DynamicTypeError)
    | Gt -> 
      (match ((evalExpr left env), (evalExpr right env)) with
      | (IntVal(a), IntVal(b)) -> BoolVal(a > b)
      | _ -> raise DynamicTypeError)
    | Cons -> 
      (match ((evalExpr left env), (evalExpr right env)) with
      | (value, ListVal(restlist)) -> ListVal(ConsVal(value, restlist))
      | _ -> raise DynamicTypeError))
  | Negate(expr) ->
    (* Evaluate inner expression, if the result is an integer, then return the negative of that integer *)
    (match (evalExpr expr env) with
    | IntVal(i) -> IntVal(-i)
    | _ -> raise DynamicTypeError)
  | If(test, iftrue, iffalse) ->
    (match (evalExpr test env) with
    | BoolVal(b) -> if b then (evalExpr iftrue env) else (evalExpr iffalse env)
    | _ -> raise DynamicTypeError)
  | Function(pattern, expression) -> FunctionVal(None, pattern, expression, env)
  | FunctionCall(funcexpr, paramexpr) -> 
    (match (evalExpr funcexpr env) with
    | FunctionVal(optionname, pattern, expression, funcenv) -> 
        let patenv = (patMatch pattern (evalExpr paramexpr env)) in
        let newenv = (match optionname with
        | Some funcname -> Env.add_binding funcname (FunctionVal(Some funcname, pattern, expression, funcenv)) funcenv
        | None -> funcenv ) in
        evalExpr expression (Env.combine_envs newenv patenv)
    | _ -> raise DynamicTypeError)
  | Match(expression, patternlist) -> 
    (match patternlist with
      [] -> raise MatchFailure (* no more patterns to match, match not found *)
      | (patt, expr)::rest ->
        (try
          let newenv = Env.combine_envs env (patMatch patt (evalExpr expression env)) 
          in evalExpr expr newenv
        with MatchFailure -> 
          (match expr with Match(_,_) -> (raise MatchFailure) | _ -> (evalExpr (Match(expression, rest)) (env)))))
  | _ -> raise (ImplementMe "expression evaluation not implemented")

(* Evaluate a declaration in the given environment.  Evaluation
   returns the name of the variable declared (if any) by the
   declaration along with the value of the declaration's expression.
*)
let rec evalDecl (d:modecl) (env:moenv) : moresult =
  match d with
    Expr(e) -> (None, evalExpr e env)
  | Let(varname, expr) -> (Some varname, evalExpr expr env)
  | LetRec(varname, pattern, expr) -> (Some varname,(FunctionVal(Some varname, pattern, expr, env)))
  | _ -> raise (ImplementMe "not implemented")
